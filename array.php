<?php


$data = array(1,2,3,4,5);
echo "<pre>";
    print_r($data);
echo "</pre>"; 

echo "<br>";
echo "<br>";

$numbers = [1,2,3,4,5,6,7,8,9,10];
echo count($numbers);

echo "<br>";
echo "<br>";

$numbers_2 = [1,2,3,4,5,6,7,8,9,9,10,10];
echo "<pre>";
    $newArr = array_unique($numbers_2);
    print_r($newArr);
echo "</pre>"; 

echo "<br>";
echo "<br>";

$numbers_3 = [
    'Prabir',
    "Karim",
    "Nayon",
    "Prabir"
];

echo "<pre>";
    $newArr = array_unique($numbers_3);
    print_r($newArr);
echo "</pre>";

echo "<br>";
echo "<br>";

$personalaInfo = [
    'name' => 'Prabir',
    'location' => 'Dhaka',
    'phone_no' => '18963897'
];

$array_keys = array_keys($personalaInfo);
$array_values = array_values($personalaInfo);
echo "<pre>";
    print_r($array_keys);
    echo "<br>";
    echo "<br>";
    print_r($array_values);
echo "</pre>";

echo "<br>";
echo "<br>";

$keys = ['name', 'location'];
$values = ['Prabir', 'Dhaka'];

echo "<pre>";
    $newArr = array_combine($keys,$values);
    print_r($newArr);
echo "</pre>";

echo "<br>";
echo "<br>";

$arr1 = ['HTML','CSS'];
$arr2 = ['PHP', 'LARAVEL'];

echo "<pre>";
$newArray = array_merge($arr1, $arr2);
print_r($newArray);
echo "</pre>";

echo "<br>";
echo "<br>";

$array1 = ['HTML', 'CSS', 'JS'];
$array2 = ['HTML', 'CSS'];

print_r(array_diff($array1, $array2));


echo "<br>";
echo "<br>";


$array1 = ['HTML', 'CSS', 'JS'];
$array2 = ['C', 'C++', 'Java'];

$newArr = array_pop($array1);
$newArr2 = array_shift($array2);

echo "<pre>";
print_r($newArray);
echo "<br>";
echo "<br>";
print_r($array1);
echo "<br>";
echo "<br>";
echo "<pre>";
print_r($array2);
echo "</pre>";

$array3 = ['Python','Ruby', 'C#'];

$newArray = in_array('C#', $array3);
echo $newArray;

echo "<br>";
echo "<br>";


if(in_array('Python', $array3)){
    echo "Yes.";
}


echo "<br>";
echo "<br>";




// $file = "readme.text";

// // if(file_exists($file))

// if(file_exists('newfile.text')){
//     // readfile($file);
//     // copy($file, 'newfile.text');
//     unlink('newfile.text');
// }
// else{
//     echo "There is no file.";
// }

// // readfile($file);


$file = fopen("readme.text", "r");
echo fread($file, 1000);
fclose($file);

//https://www.guru99.com/php-file-processing.html#2

?>



