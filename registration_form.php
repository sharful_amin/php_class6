<?php


    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        print_r($_POST);
    }



?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Registration_form</title>
  </head>
  <body>
            <div class="row">
                <div class="col-6">
                                <form action="" method="POST">
                                        <div class="container">
                                        <div class="mb-20">
                                        <label for="exampleInputName" class="form-label">Name:</label>
                                        <input type="name" class="form-control" id="exampleInputName1" name="">
                                        <div>
                                        <div class="mb-5">
                                        <label for="exampleInputEmail1" class="form-label">Email address:</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                        <div>
                                        <div class="mb-10">
                                        <label for="exampleInputPassword1" class="form-label">Password:</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1">
                                        </div>
                                        

                                        <label for="gender" class="form-label">Gender:</label>
                                        <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Male</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">Female</label>
                                        </div>
                                        
                                        
                                        <label for="country" class="form-label">Country-name:</label>
                                        <select class="form-select" aria-label="Default select example">
                                        <option selected>Country:</option>
                                        <option value="1">Bangladesh</option>
                                        <option value="2">Pakistan</option>
                                        <option value="3">India</option>
                                        </select>

                                        
                                        <label for="gender" class="form-label">Courses:</label>
                                        <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">HTML</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                        <label class="form-check-label" for="inlineCheckbox2">CSS</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                                        <label class="form-check-label" for="inlineCheckbox3">PHP</label>
                                        </div>

                                        <div class="d-grid gap-2 d-md-block">
                                        <button class="btn btn-primary w-100" type="submit">Save</button>
                                        </div>
                                </form>
                </div>

                <div class="col-6">

                </div>
            </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>

